/**
  * Created by tatiane on 25/03/17.
  */
class Node(var obj: Int, var next: Node, var last: Node) {

}

class ListQuestion5 (var first: Node) {


  def this(){
    this(null)
  }

  def add(v: Int): Unit ={

    var aux = new Node(v, null, null)

    if(first == null){
      first = aux
      first.next = first
      first.last = first
    }else{

      var n = walk()

      first.last = aux
      aux.next = first
      n.next = aux
      aux.last = n
      first = aux

    }
  }

  def walkPrint(): Unit ={

    if(first != null){
      var  aux = first
      println(first.obj)
      while(aux.next != first) {
        aux = aux.next
        println(aux.obj)

      }

      //to no fim
      //println("imprimindo voltando para testar:")
      //var contador = size
      //while (contador != 0){
      //println(aux.obj)
      //aux = aux.last
      //contador = contador-(1)
      //}

    }else{
      println("Empty.")
    }

  }

  def walkPrintRec(): Unit ={
    if(first != null)
      this.rec(first)
    else println("Empty.")
  }

  def searchInt(obj: Int): Int ={
    var item = search(obj)
    return item.obj

  }

  def isEmpty(): Int ={

    if(first == null)
      return 1
    else
      return 0
  }

  def remove(obj: Int): Int ={
    var item = new Node(Int.MaxValue, null, null)
    if(first == null){
      println("Empty.")
    }else{


      if(first.obj == obj){
        item = first
        first.last.next = first.next
        first.next.last = first.last
        first = first.next
      }else{
        var aux = search(obj)
        if(aux.obj == obj){
          item = aux
          aux.last.next = aux.next
          aux.next.last = aux.last

        }
      }
    }

    return item.obj
  }

  def removeRec(obj: Int): Int ={
    var item = new Node(Int.MaxValue, null, null)

    if(first == null){
      println("Empty.")

    }else if(first.obj == obj){
      item = first
      first.last.next = first.next
      first.next.last = first.last
      first = first.next

    }else{
      var aux = walkRec(first, obj)
      if(aux.obj == obj){
        item = aux
        aux.last.next = aux.next
        aux.next.last = aux.last

      }

    }
    return item.obj
  }

  private def search(obj: Int): Node ={
    var item = new Node(Int.MaxValue,null,null)

    if(first != null){
      var aux = first
      do{
        if(aux.obj.equals(obj)){
          item = aux
        }
        aux = aux.next
      } while (aux != first)

    }
    return item

  }

  private def walk(): Node ={

    var aux = new Node(Int.MaxValue, null, null)
    if(first != null){
      aux = first
      while(aux.next != first) {
        aux = aux.next
      }

    }else{
      println("Empty.")
    }

    return aux
  }

  private def rec(n : Node): Unit ={

    println(n.obj)
    if(n.next != first){
      rec(n.next)
    }

  }

  private def walkRec(n : Node, obj: Int): Node ={

    if(n.next != first && n.obj != obj){
      return walkRec(n.next, obj)
    }else return n
  }

}

object ListQuestion5{

  def main(args: Array[String]) {
    //questão 5.1
    val list = new ListQuestion5()

    //questão 5.2
    //list.add(2)
    //list.add(7)
    //list.add(1)
    //list.add(1)

    //questão 5.3
    //list.walkPrint()

    //questao 5.4
    //list.walkPrintRec()

    //questao 5.5
    //println(list.isEmpty())

    //questao 5.6 //considerando que nao ha dois numeros iguais na lista
    //retorna Int.maxvalue quando não existe o num na lista
    //println(list.searchInt(3))

    //questao 5.7
    //considerando que nao ha dois numeros iguais na lista
    //retorna Int.maxvalue quando não existe o num na lista
    //println("item removido: " + list.remove(2))
    //println("lista atual:")
    //list.walkPrint()

    //questao 5.8
    //considerando que nao ha dois numeros iguais na lista
    //retorna Int.maxvalue quando não existe o num na lista
    //println("item removido: " + list.removeRec(2))
    //println("lista atual:")
    //list.walkPrint()

    //questao 5.9 (nao precisa desalocar em scala)

  }

}
