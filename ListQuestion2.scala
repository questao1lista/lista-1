/**
  * Created by tatiane on 24/03/17.
  */

class Node(var obj: Int, var next: Node) {

}


class ListQuestion2(var first: Node) {

  var size = 0

  def this(){
    this(null)
  }

  def add(v: Int): Unit ={

    var aux = new Node(v, null)

    if(first == null){
      first = aux
    }else{

      if(aux.obj <= first.obj){
        aux.next = first
        first = aux
      }else {
        var n = first
        while (n.obj < aux.obj && n.next != null) {
          n = n.next
        }
        aux.next = n.next
        n.next = aux
      }
    }
    size = size+(1)

  }

  def walkPrint(): Unit ={

    if(first != null){
      var aux = first
      println(first.obj)
      while(aux.next != null) {
        aux = aux.next
        println(aux.obj)
      }
    }else{
      println("Empty.")
    }
  }

  def walkPrintRec(): Unit ={
    if(first != null){
      this.rec(first)
    }else{
      println("Empty.")
    }

  }

  def walkPrintRecInverse(): Unit ={
    if(first != null){
      this.recInverse(first)
    }else{
      println("Empty.")
    }
  }

  def searchInt(obj: Int): Int ={
    var aux = search(obj)
    return aux.obj


  }

  def isEmpty(): Int ={

    if(first == null)
      return 1
    else
      return 0
  }

  def remove(obj: Int): Int ={
    var item = new Node(Int.MaxValue, null)

    if(first ==  null){
      println("Empty.")
    }else if( first.obj == obj ){
      item = first
      first = first.next

    }else if(first.next != null){ //tem pelo menos dois elementos
      var aux = search(obj)
      var last = searchLast(obj)

      if(aux.obj == obj){
        item = aux
        last.next = aux.next
      }

    }
    if(item.obj != Int.MaxValue)
      size = size-(1)
    return item.obj
  }

  def removeRec(obj: Int): Int ={
    var item = new Node(Int.MaxValue, null)

    if(first == null){
      println("Empty.")
    }else if( first.obj == obj){
      item = first
      first = first.next
    }else if(first.next != null){ //tem pelo menos dois elementos
      var aux = walkRec(first, obj)
      var last = searchLastRec(first.next, first, obj)
      if(aux.obj == obj){
        item = aux
        last.next = aux.next
      }
    }
    if(item.obj != Int.MaxValue)
      size = size-(1)
    return item.obj
  }

  def equals(l: ListQuestion2): Boolean ={

    if(size != l.size){
      return false
    }else{
      var aux1 = first
      var aux2 = l.first
      var sent = true
      for( i <- 0 to size-1){
        if(aux1.obj != aux2.obj){
          sent = false
        }
        aux1 = aux1.next
        aux2 = aux2.next
      }
      return sent
    }
  }


  private def searchLast(obj: Int): Node ={


    var last = new Node(Int.MaxValue,null)

    var aux = first.next

    last = first

    while (aux.next != null){
      if(obj != aux.obj){
        last = aux

      }
      aux = aux.next
    }

    return last

  }

  private def rec(n : Node): Unit ={

    println(n.obj)
    if(n.next != null){
      rec(n.next)
    }

  }

  private def recInverse(n : Node): Unit ={

    if(n.next != null){
      recInverse(n.next)
    }
    println(n.obj)

  }

  private def rec(n : Node, count: Int, position: Int): Node ={

    if(n.next != null && count < position){
      return rec(n.next,count+(1),position )
    }else return n

  }

  private def search(obj: Int): Node ={
    var item = new Node(Int.MaxValue,null)

    if(first != null){
      var aux = first
      do{
        if(aux.obj.equals(obj)){
          item = aux
        }
        aux = aux.next
      } while (aux != null)

    }
    return item

  }

  private def walkRec(n : Node, obj: Int): Node ={

    if(n.next != null && n.obj != obj){
      return walkRec(n.next, obj)
    }else return n
  }

  private def searchLastRec(n : Node, last: Node, obj: Int): Node ={

    if(n.next != null && n.obj != obj){
      return searchLastRec(n.next,n, obj)
    }else return last
  }



}

object ListQuestion2{

  def main(args: Array[String]) {
    //questão 2.1
    val list = new ListQuestion2()

    //questão 2.2
    list.add(2)
    list.add(3)
    list.add(1)

    //list.add(1)

    //questão 2.3
    list.walkPrint()

    //questao 2.4
    list.walkPrintRec()

    //questao 2.5
    list.walkPrintRecInverse()

    //questao 2.6
    println(list.isEmpty())

    //questao 2.7
    //considerando que nao ha dois numeros iguais na lista
    //retorna Int.maxvalue quando não existe o num na lista
    println(list.searchInt(1))

    //questao 2.8
    //considerando que nao ha dois numeros iguais na lista
    //retorna Int.maxvalue quando não existe o num na lista
    println("item removido: " + list.remove(1))
    println("lista atual:")
    list.walkPrint()

    //questao 2.9
    //considerando que nao ha dois numeros iguais na lista
    //retorna Int.maxvalue quando não existe o num na lista
    println("item removido: " + list.removeRec(1))
    println("lista atual:")
    list.walkPrint()

    //questao 2.10 (nao precisa desalocar em scala)

    //questao 2.11
    val list2 = new ListQuestion2()
    list2.add(2)
    list2.add(3)
    list2.add(1)

    //list2.add(2)
    println(list.equals(list2))

  }

}