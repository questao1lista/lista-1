/**
  * Created by tatiane on 25/03/17.
  */
class Node(var obj: Int, var next: Node) {

}

class ListQuestion4 (var first: Node) {


  def this(){
    this(null)
  }

  def add(v: Int): Unit ={

    var aux = new Node(v, null)

    if(first == null){
      first = aux
      first.next = first
    }else{

      var n = walk()

      aux.next = first
      n.next = aux
      first = aux

    }
  }

  def walkPrint(): Unit ={

    if(first != null){
      var  aux = first
      println(first.obj)
      while(aux.next != first) {
        aux = aux.next
        println(aux.obj)

      }

    }else{
      println("Empty.")
    }

  }

  def walkPrintRec(): Unit ={
    if(first != null)
    this.rec(first)
    else println("Empty.")
  }

  def searchInt(obj: Int): Int ={
    var item = search(obj)
    return item.obj

  }

  def isEmpty(): Int ={

    if(first == null)
      return 1
    else
      return 0
  }

  def remove(obj: Int): Int ={

    var item = new Node(Int.MaxValue, null)

    if(first == null){
      println("Empty.")
    }else if(first.obj == obj){
          var aux = walk()
          item = first
          first = first.next
          aux.next = first
      }else if(first.next != null){ //ao menos dois
        var aux = search(obj)
        var last = searchLast(obj)

        if(aux.obj == obj){
          item = aux
          last.next = aux.next
        }

    }

    return item.obj
  }

  def removeRec(obj: Int): Int ={
    var item = new Node(Int.MaxValue, null)

    if(first == null){
      println("Empty.")
    }else if(first.obj == obj){
      var aux = walk()
      item = first
      first = first.next
      aux.next = first
    }else if(first.next != null){ //ao menos dois
      var aux = walkRec(first, obj)
      var last = searchLastRec(first.next, first, obj)

      if(aux.obj == obj){
        item = aux
        last.next = aux.next
      }

    }

    return item.obj
  }


  private def search(obj: Int): Node ={
    var item = new Node(Int.MaxValue,null)

    if(first != null){
      var aux = first
      do{
        if(aux.obj.equals(obj)){
          item = aux
        }
        aux = aux.next
      } while (aux != first)

    }
    return item

  }

  private def searchLast(obj: Int): Node ={


    var last = new Node(Int.MaxValue,null)

    var aux = first.next

      last = first

    while (aux.next != first){
        if(obj != aux.obj){
          last = aux

        }
        aux = aux.next
      }

    return last

  }


  private def walk(): Node ={

    var aux = new Node(Int.MaxValue, null)
    if(first != null){
      aux = first
      while(aux.next != first) {
        aux = aux.next
      }

    }else{
      println("Empty.")
    }

    return aux
  }

  private def rec(n : Node): Unit ={

    println(n.obj)
    if(n.next != first){
      rec(n.next)
    }

  }

  private def walkRec(n : Node, obj: Int): Node ={

    if(n.next != first && n.obj != obj){
      return walkRec(n.next, obj)
    }else return n
  }

  private def searchLastRec(n : Node, last: Node, obj: Int): Node ={

    if(n.next != first && n.obj != obj){
      return searchLastRec(n.next,n, obj)
    }else return last
  }





}

object ListQuestion4{

  def main(args: Array[String]) {
    //questão 4.1
    val list = new ListQuestion4()

    //questão 4.2
    list.add(2)
    list.add(7)
    list.add(1)


    //questão 4.3
    //list.walkPrint()

    //questao 4.4
    //list.walkPrintRec()

    //questao 4.5
    //println(list.isEmpty())

    //questao 4.6 //considerando que nao ha dois numeros iguais na lista
    //retorna Int.maxvalue quando não existe o num na lista
    //println(list.searchInt(1))

    //questao 4.7
    //considerando que nao ha dois numeros iguais na lista
    //retorna Int.maxvalue quando não existe o num na lista
    //println("item removido: " + list.remove(3))
    //println("lista atual:")
    //list.walkPrint()

    //questao 4.8
    //considerando que nao ha dois numeros iguais na lista
    //retorna Int.maxvalue quando não existe o num na lista
    //println("item removido: " + list.removeRec(3))
    //println("lista atual:")
    //list.walkPrint()

    //questao 4.9 (nao precisa desalocar em scala)

  }

}
