/**
  * Created by tatiane on 25/03/17.
  */

 trait ContaBancaria{

  var saldo = 0.0
  var numero = 1

  def saque( valor: Double): Unit ={
      return saldo = saldo-(valor)
  }

  def deposito(valor: Double ): Unit ={
    return saldo = saldo+(valor)
  }



}

case class ContaB() extends ContaBancaria {


}

case class Poupanca() extends ContaBancaria{

   def rendimento(juros: Double): Unit ={
     return saldo = saldo * (1.0 + juros)
   }

}

case class Fidelidade(var bonus: Double) extends ContaBancaria{

  override def deposito(valor: Double): Unit =
  {
    bonus = valor*(0.01)
    saldo = saldo+(valor)
  }

  def redimentoBonus(): Unit ={
    saldo = saldo+(bonus)
    bonus = 0
  }
}


class Node(var obj: ContaBancaria, var next: Node) {

}


class ListQuestion6 (var first: Node) {


  def this(){
    this(null)
  }

  def add(obj: ContaBancaria): Unit ={

    var aux = new Node(obj, null)
    aux.next = first
    first = aux;

  }

  def addOrdenado(obj: ContaBancaria): Unit ={

    var aux = new Node(obj, null)

    if(first == null){
      first = aux
    }else{

      if(aux.obj.numero <= first.obj.numero){
        aux.next = first
        first = aux
      }else {
        var n = first
        while (n.obj.numero < aux.obj.numero && n.next != null) {
          n = n.next
        }
        aux.next = n.next
        n.next = aux
      }
    }

  }

  def walkPrint(): Unit ={

    if(first != null){
      var aux = first
          println("---- Conta Bancaria----")
          println(aux.obj.numero)
          println(aux.obj.saldo)
          println("-----------------------")

      while(aux.next != null) {
        aux = aux.next
          println("---- Conta Bancaria----")
          println(aux.obj.numero)
          println(aux.obj.saldo)
          println("-----------------------")
      }
    }else{
      println("Nao existem contas cadastradas.")
    }
  }

  def walkPrintRec(): Unit ={
    if(first != null){
      this.rec(first)
    }else{
      println("Empty.")
    }

  }

  def walkPrintRecInverse(): Unit ={
    if(first != null){
      this.recInverse(first)
    }else{
      println("Empty.")
    }
  }

  def searchNumConta(obj: Int): ContaBancaria ={
    var aux = search(obj)
    return aux.obj


  }

  def isEmpty(): Int ={

    if(first == null)
      return 1
    else
      return 0
  }

  def remove(obj: Int): ContaBancaria ={
    var item = new Node(null, null)

    if(first ==  null){
      println("Empty.")
    }else if( first.obj.numero == obj ){
      item = first
      first = first.next

    }else if(first.next != null){ //tem pelo menos dois elementos
      var aux = search(obj)
      var last = searchLast(obj)

      if(aux.obj.numero == obj){
        item = aux
        last.next = aux.next
      }
    }
    return item.obj
  }

  def removeRec(obj: Int): ContaBancaria ={
    var item = new Node(null, null)

    if(first == null){
      println("Empty.")
    }else if( first.obj.numero == obj){
      item = first
      first = first.next
    }else if(first.next != null){ //tem pelo menos dois elementos
      var aux = walkRec(first, obj)
      var last = searchLastRec(first.next, first, obj)
      if(aux.obj.numero == obj){
        item = aux
        last.next = aux.next
      }
    }

    return item.obj
  }

  private def searchLast(obj: Int): Node ={


    var last = new Node(null,null)

    var aux = first.next

    last = first

    while (aux.next != null){
      if(obj != aux.obj.numero){
        last = aux
      }
      aux = aux.next
    }

    return last

  }

  private def rec(n : Node): Unit ={

      println("---- Conta Bancaria----")
      println(n.obj.numero)
      println(n.obj.saldo)
      println("-----------------------")

    if(n.next != null){
      rec(n.next)
    }

  }

  private def recInverse(n : Node): Unit ={

    if(n.next != null){
      recInverse(n.next)
    }
      println("---- Conta Bancaria----")
      println(n.obj.numero)
      println(n.obj.saldo)
      println("-----------------------")

  }

  private def rec(n : Node, count: Int, position: Int): Node ={

    if(n.next != null && count < position){
      return rec(n.next,count+(1),position )
    }else return n

  }

  private def search(obj: Int): Node ={
    var item = new Node(null,null)

    if(first != null){
      var aux = first
      do{
        if(aux.obj.numero.equals(obj)){
          item = aux
        }
        aux = aux.next
      } while (aux != null)

    }
    return item

  }

  private def walkRec(n : Node, obj: Int): Node ={

    if(n.next != null && n.obj.numero != obj){
      return walkRec(n.next, obj)
    }else return n
  }

  private def searchLastRec(n : Node, last: Node, obj: Int): Node ={

    if(n.next != null && n.obj.numero != obj){
      return searchLastRec(n.next,n, obj)
    }else return last
  }

}


object ListQuestion6 {


  def main(args: Array[String]) {
    val list = new ListQuestion6()
    var cont = 1
    var resp = "11"
    while (resp != "sair") {

      println("Digite:")
      println("1. Inserir uma conta bancaria;")
      println("2. Inserir uma conta poupança;")
      println("3. Inserir uma conta fidelidade;")
      println("4. Realizar credito em uma determinada conta;")
      println("5. Realizar debito em uma determinada conta;")
      println("6. Consultar o saldo de uma conta;")
      println("7. Consultar o bonus de uma conta fidelidade;")
      println("8. Realizar uma transferência entre duas contas;")
      println("9. Render juros de uma conta poupança;")
      println("10. Render bônus de uma conta fidelidade;")
      println("11. Remover uma conta;")
      println("12. Imprimir numero e saldo de todas as contas cadastradas;")
      println("Para sair do sistema digite: sair")

      println("Qual operação deseja efetuar: ")
      resp = scala.io.StdIn.readLine()

      if(resp.equals("1")){
        var c = new ContaB()
        c.numero = cont
        c.saldo = 0
        list.add(c)
        //list.addOrdenado(c)
        println("Sua conta foi cadastrada com sucesso! O numero da sua conta é " + cont )
        cont = cont+(1)

      }else if(resp.equals("2")){
        var c = new Poupanca()
        c.numero = cont
        c.saldo = 0
        list.add(c)
        //list.addOrdenado(c)
        println("Sua conta foi cadastrada com sucesso! O numero da sua conta é " + cont )
        cont = cont+(1)

      }else if(resp.equals("3")){
        var c = new Fidelidade(0)
        c.numero = cont
        c.saldo = 0
        list.add(c)
        //list.addOrdenado(c)
        println("Sua conta foi cadastrada com sucesso! O numero da sua conta é " + cont )
        cont = cont+(1)

      }else if(resp.equals("4")){
        println("Qual o numero da conta?")
        var num = scala.io.StdIn.readInt()
        println("Qual o valor a ser depositado?")
        var valor = scala.io.StdIn.readDouble()
        var aux = list.searchNumConta(num)
        if(aux != null){
          aux.deposito(valor)
          println("Deposito efetuado com sucesso. Seu saldo e: " + aux.saldo)
        }else println("Conta nao encontrada")


      }else if(resp.equals("5")){
        println("Qual o numero da conta?")
        var num = scala.io.StdIn.readInt()
        println("Qual o valor a ser sacado?")
        var valor = scala.io.StdIn.readDouble()
        var aux = list.searchNumConta(num)
        if(aux != null){
          if (aux.saldo >= valor){
            aux.saque(valor)
            println("Saque realizado com sucesso")
          }else println("Saldo insuficiente")
          println("Saque efetuado com sucesso. Seu saldo e: " + aux.saldo)
        }else println("Conta nao encontrada")

      }else if(resp.equals("6")){
        println("Qual o numero da conta?")
        var num = scala.io.StdIn.readInt()
        var aux = list.searchNumConta(num)
        if(aux != null){
          println("Busca efetuada com sucesso. Seu saldo e: " + aux.saldo)
        }else println("Conta nao encontrada")

      }else if(resp.equals("7")){

        println("Qual o numero da conta?")
        var num = scala.io.StdIn.readInt()
        var aux = list.searchNumConta(num)
        if(aux != null){
          if(aux.isInstanceOf[Fidelidade]){
            var c: Fidelidade = aux.asInstanceOf[Fidelidade]
            println("Bonus: "+c.bonus)
          }else{
            println("Esta conta nao e fidelidade")
          }

        }else println("Conta nao encontrada")

      }else if(resp.equals("8")){
        println("Qual o numero da conta para debito?")
        var num = scala.io.StdIn.readInt()
        var aux = list.searchNumConta(num)

        if(aux != null){

          println("Qual o numero da conta para credito?")
          var num2 = scala.io.StdIn.readInt()
          var aux2 = list.searchNumConta(num2)
          if(aux2 != null){

            println("Valor que devera ser transferido?")
            var valor = scala.io.StdIn.readDouble()

            if(aux.saldo >= valor){
              aux.saque(valor)
              aux2.deposito(valor)
              println("Tranferencia realizada com sucesso")

            }else println("Saldo insuficiente")

          }else println("Conta para credito nao encontrada")

        }else println("Conta para debito nao encontrada")


      }else if(resp.equals("9")){
        println("Qual o numero da conta?")
        var num = scala.io.StdIn.readInt()
        var aux = list.searchNumConta(num)
        if(aux != null){
          if(aux.isInstanceOf[Poupanca]){
            var c: Poupanca = aux.asInstanceOf[Poupanca]
            c.rendimento(0.01)
            println("Saldo com rendimento: " + c.saldo)
          }else{
            println("Esta conta nao e poupanca")
          }

        }else println("Conta nao encontrada")


      }else if(resp.equals("10")){
        println("Qual o numero da conta?")
        var num = scala.io.StdIn.readInt()
        var aux = list.searchNumConta(num)
        if(aux != null){
          if(aux.isInstanceOf[Fidelidade]){
            var c: Fidelidade = aux.asInstanceOf[Fidelidade]
            println("Bonus: " + c.bonus)
            c.redimentoBonus()
            println("Saldo apos rendimento: " + c.saldo)
            println("Bonus atual: " + c.bonus)
          }else{
            println("Esta conta nao e poupanca")
          }

        }else println("Conta nao encontrada")


      }else if(resp.equals("11")){
        println("Qual o numero da conta?")
        var num = scala.io.StdIn.readInt()
        var aux = list.remove(num)
        if(aux != null){
          println("Conta bancaria numero " + aux.numero + " removida com sucesso.")

        }else println("Conta nao encontrada")


      }else if(resp.equals("12")){
        list.walkPrint();
      }

      }

  }
}
