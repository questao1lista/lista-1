/**
  * Created by tatiane on 24/03/17.
  */
class Node(var obj: Int, var next: Node, var last: Node) {

}

class ListQuestion3 (var first: Node) {

  var size = 0


  def this(){
    this(null)
  }

  def add(v: Int): Unit ={

    var aux = new Node(v, null, null)

    if(first == null){
      first = aux
    }else{
      aux.next = first
      first.last = aux
      first = aux
    }
    size = size+(1)
  }

  def walkPrint(): Unit ={

    if(first != null){
      var  aux = first
      println(first.obj)
      while(aux.next != null) {
        aux = aux.next
        println(aux.obj)
      }

      //to no fim
      //println("imprimindo voltando para testar:")
      //var contador = size
      //while (contador != 0){
      //println(aux.obj)
      //aux = aux.last
      //contador = contador-(1)
      //}

    }else{
      println("Empty.")
    }

  }

  def walkPrintRec(): Unit ={
    if(first != null)
      this.rec(first)
    else println("Empty.")
  }

  def walkPrintRecInverse(): Unit ={
    if(first != null){
      this.recInverse(first)
    }else{
      println("Empty.")
    }
  }

  def searchInt(obj: Int): Int ={
    var item = search(obj)
    return item.obj

  }

  def isEmpty(): Int ={

    if(first == null)
      return 1
    else
      return 0
  }

  def remove(obj: Int): Int ={
    var item = new Node(Int.MaxValue, null, null)
    if(first == null){
      println("Empty.")
    }else if(first.obj == obj){
        item = first
        first = first.next
      }else if(first.next != null) {
      var aux = search(obj)
      if (aux.obj == obj) {
        item = aux
        if (aux.next != null) {
          aux.last.next = aux.next
          aux.next.last = aux.last
        } else {
          aux.last.next = null
        }
      }
    }
    if(item.obj == Int.MaxValue )
      size = size-(1)
    return item.obj
  }

  def removeRec(obj: Int): Int ={
    var item = new Node(Int.MaxValue, null, null)

    if(first == null){
      println("Empty.")

    }else if(first.obj == obj){
      item = first
      first = first.next
    }else if(first.next != null){
      var aux = walkRec(first, obj)
      if(aux.obj == obj){
        item = aux
        if(aux.next != null)
        {
          aux.last.next = aux.next
          aux.next.last = aux.last
        }else{
          aux.last.next = null
        }
      }
    }
    if(item.obj == Int.MaxValue )
      size = size-(1)
    return item.obj
  }

  def equals(l: ListQuestion3): Boolean ={

    if(size != l.size){
      return false
    }else{
      var aux1 = first
      var aux2 = l.first
      var sent = true
      for( i <- 0 to size-1){
        if(aux1.obj != aux2.obj){
          sent = false
        }
        aux1 = aux1.next
        aux2 = aux2.next
      }
      return sent
    }
  }

  private def search(obj: Int): Node ={
    var item = new Node(Int.MaxValue,null,null)

    if(first != null){
      var aux = first
      do{
        if(aux.obj.equals(obj)){
          item = aux
        }
        aux = aux.next
      } while (aux != null)

    }
    return item

  }

  private def walk(): Node ={

    var aux = new Node(Int.MaxValue, null, null)
    if(first != null){
      aux = first
      while(aux.next != null) {
        aux = aux.next
      }

    }else{
      println("Empty.")
    }

    return aux
  }

  private def rec(n : Node): Unit ={

    println(n.obj)
    if(n.next != null){
      rec(n.next)
    }

  }

  private def walkRec(n : Node, obj: Int): Node ={

    if(n.next != null && n.obj != obj){
      return walkRec(n.next, obj)
    }else return n
  }

  private def recInverse(n : Node): Unit ={

    if(n.next != null){
      recInverse(n.next)
    }
    println(n.obj)

  }
}

object ListQuestion3{

  def main(args: Array[String]) {
    //questão 3.1
    val list = new ListQuestion3()

    //questão 3.2
    list.add(2)
    list.add(3)
    list.add(1)
    //list.add(1)

    //questão 3.3
   // list.walkPrint()

    //questao 3.4
    //list.walkPrintRec()

    //questao 3.5
    //list.walkPrintRecInverse()

    //questao 3.6
    //println(list.isEmpty())

    //questao 3.7
    //considerando que nao ha dois numeros iguais na lista
    //retorna Int.maxvalue quando não existe o num na lista
    //println(list.searchInt(3))

    //questao 3.8
    //considerando que nao ha dois numeros iguais na lista
    //retorna Int.maxvalue quando não existe o num na lista
    //println("item removido: " + list.remove(3))
    //println("lista atual:")
    //list.walkPrint()

    //questao 2.9
    //considerando que nao ha dois numeros iguais na lista
    //retorna Int.maxvalue quando não existe o num na lista
    //println("item removido: " + list.removeRec(0))
    //println("lista atual:")
    //list.walkPrint()

    //questao 2.10 (nao precisa desalocar em scala)

    //questao 2.11
    //val list2 = new ListQuestion3()
    //list2.add(2)
    //list2.add(3)
    //list2.add(1)
    //list2.add(5)
    //list2.add(1)
    //list2.add(3)
    //list2.add(2)
    //println(list.equals(list2))

  }

}
